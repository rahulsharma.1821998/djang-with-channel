# routing.py
from django.urls import path
from .consumers import Client1, Client2

websocket_urlpatterns = [
    path('ws/client1/', Client1.as_asgi()),
    path('ws/client2/', Client2.as_asgi()),
]
