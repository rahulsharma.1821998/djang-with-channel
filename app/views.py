from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.response import Response
from .models import Student
from .serializers import StudentSerializer



# Create your views here.
def getData(request):
    student_data = Student.objects.all()
    student_serializer_data = StudentSerializer(student_data, many=True)
    return Response(student_serializer_data.data)