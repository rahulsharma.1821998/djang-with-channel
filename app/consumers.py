# consumers.py
from channels.generic.websocket import AsyncWebsocketConsumer, WebsocketConsumer,JsonWebsocketConsumer
import json
import requests 
from django.http import HttpResponse
import time
import asyncio
import random
import time
from rest_framework.viewsets import ModelViewSet
from channels.db import database_sync_to_async
from .views import getData


class Client1(AsyncWebsocketConsumer):    
    async def connect(self):
        await self.accept()

    async def receive(self, text_data):
        while True:
            #send random number
            # random_number = random.randint(0,100)
            # await self.send(text_data=f"{random_number} from client 1")
            
            #send list
            # data = [1,2,3,4,5,6,7,8,9]
            # convertedData = json.dumps(data)
            # await self.send(convertedData)
            
            #send dict
            # data = {
            #     "name": "rahul",
            #     "eduction": ["MSCIT","BCA"],
            #     "exprience": {
            #         "python": "django",
            #         "node js": "express.js,express.ts",
            #         "database": "mysql,mongodb,postgres sql,influx db"
            #     }
            # }
            # convertedData = json.dumps(data)
            # await self.send(convertedData)
            
            # get data from database
            student_data = await database_sync_to_async(getData)(self.scope)
            student_serializer_data = json.dumps(student_data.data)
            await self.send(student_serializer_data)
            await asyncio.sleep(5)
        
    async def disconnect(self, close_code):
        await self.close()

class Client2(AsyncWebsocketConsumer):
    async def connect(self):
        await self.accept()

    async def receive(self, text_data):
        while True:
            random_number = random.randint(0,100)
            await self.send(text_data=f"{random_number} from client 2")
            await asyncio.sleep(5)
        
    async def disconnect(self, close_code):
        await self.close()
